app.controller("lpenjualanbrgCtrl", function ($scope, Data, $rootScope) {
    $scope.filter = {};
    $scope.tampilkan = false;
    $scope.filter.tanggal = new Date();
    // $scope.form = {
    //     total : 0,
    //     jmlbrg : 0
    // }

    $scope.getLaporan = function (filter) {
        console.log(filter);
        Data.get("l_penjualan_barang/view", filter).then(function (data) {
            if (data.status_code == 200) {
                $scope.tampilkan = true;
                $scope.dataPenjualan = data.data.list;
                console.log($scope.dataPenjualan);
                console.log(data);
                $scope.Total = data.data.totalsemua;
                $scope.Rincian = data.data.rincian;
                // $scope.getSubTotal();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }

        });
    };

    Data.get("l_penjualan_barang/barang").then(function (data) {
        if (data.status_code == 200) {
            $scope.listBarang = data.data.list;
            console.log($scope.listBarang);
            console.log(data);
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }
    });

    Data.get("l_penjualan_barang/customer").then(function (data) {
        if (data.status_code == 200) {
            $scope.listCustomer = data.data.list;
            console.log($scope.listCustomer);
            console.log(data);
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }
    });

    // $scope.getSubTotal = function () {
    //     var total = 0;
    //     var jmlbrg = 0;
    //     angular.forEach($scope.dataPenjualan, function (value, key) {
    //         value.SubTotal = value.jumlah_jual * value.harga_satuan;
    //         total = total + value.SubTotal;
    //         jmlbrg = jmlbrg + value.jumlah_jual;
    //     });
    //     console.log(jmlbrg);
    //     console.log(total);
    //     $scope.form.total = total;
    //     $scope.form.jmlbrg = jmlbrg;
    // };

    $scope.view = function (is_export, is_print) {

        var param = {
            is_export: is_export,
            is_print: is_print,
            tanggal: moment($scope.filter.tanggal).format('YYYY-MM-DD'),
        };

        if (is_export == 0 && is_print == 0) {
            Data.get(control_link + '/laporan', param).then(function (response) {
                if (response.status_code == 200) {
                    $scope.data = response.data.data;
                    $scope.detail = response.data.detail;

                    $scope.tampilkan = true;
                } else {
                    $scope.tampilkan = false;
                }
            });
        } else {
            Data.get('site/base_url').then(function (response) {
//                console.log(response)
                window.open(response.data.base_url + "api/l_penjualan_barang/view?" + $.param(param), "_blank");
            });
        }
    };
});