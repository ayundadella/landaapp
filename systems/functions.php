<?php

function twigView()
{
    $view = new \Slim\Views\Twig('views');
    return $view;
}

function modulUrl()
{
    $port = !empty($_SERVER['SERVER_PORT']) ? ":" . $_SERVER['SERVER_PORT'] : "";
    $a = "http://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
    $a = str_replace($_SERVER['PATH_INFO'], '', $a);
    $a = substr($a, 0, strpos($a, "?"));
//    return $a . "/" . config('MODUL_ACC')['PATH'];
}
/**
 * Set path untuk slim twig view
 */
function twigViewPath()
{
    $view = new \Slim\Views\Twig(config('MODUL_ACC')['PATH'] . '/view');
    return $view;
}
