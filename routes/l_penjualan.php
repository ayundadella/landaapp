<?php
/**
 * Menampilkan seluruh data penjualan
 */

$app->get("/l_penjualan/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $tanggal = $params['bulan'];
    $tanggal_int = strtotime($tanggal);
    $tanggal_awal = date('Y-m-01', $tanggal_int);
    $tanggal_akhir = date('Y-m-t', $tanggal_int);

    $db->select("m_barang.id AS m_barang_id,
                m_barang.nama AS barang_nama,
                t_penjualan_det.jumlah AS jumlah_jual,
                SUM(t_penjualan_det.jumlah) AS penjumlahan,
                t_penjualan.tanggal AS penjualan_tanggal")
        ->from("t_penjualan")
        ->join("left join", "t_penjualan_det", "t_penjualan_det.t_penjualan_id=t_penjualan.id")
        ->join("left join", "m_barang", "t_penjualan_det.m_barang_id=m_barang.id")
        ->where("tanggal", ">=", $tanggal_awal)
        ->where("tanggal", "<=", $tanggal_akhir)
        ->groupby("m_barang_id, t_penjualan.tanggal");

    if (isset($params["pilihBarang"]) && !empty($params["pilihBarang"])) {
        $db->where("m_barang.id", "=", $params["pilihBarang"]);
    }
    $models = $db->findAll();
//    print_r($models);
//    die;


    $result = [];
    foreach ($models as $key => $value) {
        $result[$value->m_barang_id]["m_barang_id"] = $value->m_barang_id;
        $result[$value->m_barang_id]["nama_barang"] = $value->barang_nama;
        $result[$value->m_barang_id]["dataPenjualan"][$value->penjualan_tanggal] = $value->penjumlahan;
    }

    $listDate = [];
    while (strtotime($tanggal_awal) <= strtotime($tanggal_akhir)) {
        $listDate[] = $tanggal_awal;
        $tanggal_awal = date("Y-m-d", strtotime("+1 day", strtotime($tanggal_awal)));
    }

    $jumlahperHari = [];
    $jumlahperBulan = [];
    foreach ($result as $key => $value) {
        foreach ($listDate as $dates) {
            if (empty($value["dataPenjualan"][$dates])) {
                $result[$key]["dataPenjualan"][$dates]= 0;
            }
            @$jumlahperBulan[$value["m_barang_id"]] += $result[$key]["dataPenjualan"][$dates];
            @$jumlahperHari[$dates] += $result[$key]["dataPenjualan"][$dates];
        }
        ksort($result[$key]["dataPenjualan"]); //mengurutkan tanggal
    }

//    print_r($jumlahperHari);
//    die;

    $totalItem = $db->count();
    return successResponse($response, ["list" => $result, "listTanggal" => $listDate,
        "hari" => $jumlahperHari, "bulan" => $jumlahperBulan, "totalItems" => $totalItem]);

});

$app->get("/l_penjualan/barang", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_barang");

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);

});


