<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "m_jurusan_id" => "required",
        "m_kelas_id" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/t_kelompok_kelas/jurusan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_jurusan")
        ->where("nama","like",$params["nama"]);
    $models=$db->findAll();
    return successResponse($response,["list"=>$models]);
});

$app->get("/t_kelompok_kelas/kelas", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_kelas")
        ->where("nama", "like", $params["data"])
        ->andWhere("m_jurusan_id", "=", $params["jurusan_id"]);

    $model = $db->findAll();
    return successResponse($response, ["list" => $model]);
});

$app->get("/t_kelompok_kelas/getListKelas/{id}", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $id = $request->getAttribute('id');
    $db->select("m_kelas.*")
        ->from("m_kelas")
        ->join("left join", "m_jurusan", "m_kelas.m_jurusan_id=m_jurusan.id")
        ->where("m_jurusan_id", "=", $id);

    $model = $db->findAll();
    return successResponse($response, ["list" => $model]);
});

$app->get("/t_kelompok_kelas/mahasiswa", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_mahasiswa")->where("nama", "like", $params["nama"]);

    $model = $db->findAll();
    return successResponse($response, ["list" => $model]);
});

/**
 * Ambil detail t kelompok kelas
 */
$app->get("/t_kelompok_kelas/view/{id}", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $id = $request->getAttribute('id');
    $db->select("t_kelompok_kelas_det.*, m_mahasiswa.nama AS nama_mahasiswa")
        ->from("t_kelompok_kelas_det")
        ->join("left join", "m_mahasiswa", "t_kelompok_kelas_det.m_mahasiswa_id=m_mahasiswa.id")
        ->where("t_kelompok_kelas_id", "=", $id);
    $models = $db->findAll();

    foreach ($models as $key => $value) {
        $models[$key]->m_mahasiswa_id = [
            "id" => $value->m_mahasiswa_id,
            "nama" => $value->nama_mahasiswa
        ];
    }
    return successResponse($response, $models);
});
/**
 * Ambil semua t kelompok kelas
 */
$app->get("/t_kelompok_kelas/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("t_kelompok_kelas.*, m_jurusan.nama AS namajurusan, m_kelas.nama AS namaKelas")
        ->from("t_kelompok_kelas")
        ->join("left join", "m_jurusan", "t_kelompok_kelas.m_jurusan_id=m_jurusan.id")
        ->join("left join", "m_kelas", "t_kelompok_kelas.m_kelas_id=m_kelas.id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();

    foreach ($models as $key => $value) {
        $models[$key]->m_jurusan_id = [
            "id" => $value->m_jurusan_id,
            "nama" => $value->namajurusan
        ];

        $models[$key]->m_kelas_id = [
            "id" => $value->m_kelas_id,
            "nama" => $value->namaKelas
        ];
    }

    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save t kelompok kelas
 */
$app->post("/t_kelompok_kelas/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data["data"]);
    if ($validasi === true) {
        try {
            $data['data']['m_jurusan_id'] = $data['data']['m_jurusan_id']['id'];
            $data['data']['m_kelas_id'] = $data['data']['m_kelas_id']['id'];
            if (isset($data["data"]["id"])) {
                $model = $db->update("t_kelompok_kelas", $data["data"], ["id" => $data["data"]["id"]]);
                $db->delete("t_kelompok_kelas_det", ["t_kelompok_kelas_id" => $data["data"]["id"]]);
            } else {
                $model = $db->insert("t_kelompok_kelas", $data["data"]);
            }
            /**
             * Simpan detail
             */
            if (isset($data["detail"]) && !empty($data["detail"])) {
                foreach ($data["detail"] as $key => $val) {
                    $detail["m_mahasiswa_id"] = $val["m_mahasiswa_id"]["id"];
                    $detail["t_kelompok_kelas_id"] = $model->id;

                    $db->insert("t_kelompok_kelas_det", $detail);
                }
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status t kelompok kelas
 */
$app->post("/t_kelompok_kelas/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['m_jurusan_id'] = $data['m_jurusan_id']['id'];
            $data['m_kelas_id'] = $data['m_kelas_id']['id'];
            $model = $db->update("t_kelompok_kelas", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
