<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "m_kategori_id" => "required",
        "nama" => "required",
        "stok" => "required",
        "satuan" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/m_barang/kategori", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_kategori")
        ->where("nama", "like", $params["nama"]);
    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});
/**
 * Ambil semua m barang
 */
$app->get("/m_barang/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_barang.*, 
                 m_kategori.nama AS nama_kategori")
        ->from("m_barang")
        ->join("left join", "m_kategori", "m_barang.m_kategori_id=m_kategori.id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    foreach ($models as $key => $value) {
        $models[$key]->m_kategori_id = [
            "id"=> $value->m_kategori_id,
            "nama"=> $value->nama_kategori
        ];
    }
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save m barang
 */
$app->post("/m_barang/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['m_kategori_id'] = $data['m_kategori_id']['id'];
            if (isset($data["id"])) {
                $model = $db->update("m_barang", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_barang", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status m barang
 */
$app->post("/m_barang/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_barang", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
