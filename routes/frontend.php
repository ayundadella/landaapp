<?php
$app->get('/',function($request,$response) {
    $db = $this->db;

    return $this->view->render($response, 'frontend/home.twig', [
        'page' => 'home',
        'keyword' => 'keyword',
        'description' => 'Deskripsi Web',
    ]);
});