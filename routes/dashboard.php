<?php
$app->get("/dashboard/barChart", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_barang.nama AS barang_nama,
                SUM(t_penjualan_det.jumlah) AS penjumlahan")
        ->from("t_penjualan")
        ->join("left join", "t_penjualan_det", "t_penjualan_det.t_penjualan_id=t_penjualan.id")
        ->join("left join", "m_barang", "t_penjualan_det.m_barang_id=m_barang.id")
        ->where("t_penjualan.status", "=", "tersimpan")
        ->groupby("m_barang.id")
        ->orderby("penjumlahan DESC")
        ->limit('3');

    $nama_barang = [];
    $jumlah_barang = [];

    $models = $db->findAll();

    foreach ($models as $key => $value) {
        array_push($nama_barang, $value->barang_nama);
        array_push($jumlah_barang, $value->penjumlahan);
    }

//    print_r($models);
//    die;
    $jumlah_barang = array_map('intval', $jumlah_barang);
    array_multisort($jumlah_barang, SORT_ASC, $nama_barang, SORT_DESC, $models);

    return successResponse($response, ["nama_barang" => $nama_barang,
        "jumlah_barang" => $jumlah_barang]);
});


$app->get("/dashboard/lineChart", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_barang.nama AS barang_nama,
                m_kategori.id AS m_kategori_id,
                m_kategori.nama AS kategori_nama,
                MONTH(t_penjualan.tanggal) AS bulan_penjualan,
                SUM(t_penjualan_det.jumlah) AS jumlah_jual")
        ->from("t_penjualan")
        ->join("left join", "t_penjualan_det", "t_penjualan_det.t_penjualan_id=t_penjualan.id")
        ->join("left join", "m_barang", "t_penjualan_det.m_barang_id=m_barang.id")
        ->join("left join", "m_kategori", "m_barang.m_kategori_id=m_kategori.id")
        ->where("t_penjualan.status", "=", "tersimpan")
        ->groupby("m_kategori.id, bulan_penjualan")
        ->orderby("bulan_penjualan DESC");

    $models = $db->findAll();
//    print_r($models);
//    die;

    $bulan = range(1, 12);
    $kategori = [];
    $result = [];

    foreach ($models as $key => $value) {
        $result[$value->kategori_nama][$value->bulan_penjualan] = intval($value->jumlah_jual);
    }

    $series = [];
    //memanggil dan menampilkan kategori barang
    foreach ($result as $key => $value) {
        $new = [
            'values' => array_values($result[$key]),
            'text' => $key,
        ];
        foreach ($bulan as $k => $v) {
            if (!isset($result[$key][$v])) {
                $result[$key][$v] = 0;
            }
        }
        ksort($result[$key]);
        $new['values'] = array_values($result[$key]);
        $series[] = $new;
    }
//    print_r($series);
//    die;

    return successResponse($response, ["nama_kategori" => $kategori,
        "result" => $result, "series" => $series]);
});


